# Introduction to Machine-Learning and Deep Learning using scikit learn and tenserflow
In this repository you can see examples and exercises I solved to learn and get the Idea of machine Learning and deep learning mostly using scikit-learn and TensorFlow.
# References
titles and basic idead: https://github.com//ageron/handson-ml/ <br/>
O'Reilly [book Hands-on Machine Learning with Scikit-Learn and TensorFlow](http://shop.oreilly.com/product/0636920052289.do)<br/>
scikit learn library: https://scikit-learn.org/<br/>
scikit learn documentation: https://scikit-learn.org/stable/documentation.html<br/>
